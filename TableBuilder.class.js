var TableBuilder = (function() {

    var _ajax_url = '';
    var _table_details = [];
    var _actions = [];
    var _rows = [];
    var _actions = [];
    var _table_id;
    var _table_class;
    var _display_row_num = false;

    var headers = [];
    var num_cols = 0;
    var num_headers = 0;
    var num_rows = 0;
    var num_actions = 0;

    //constructor
    /***
     * @param (array) table_elements  : {'id' : 'CSS ID', 'class' : 'CSS class', 'container_id': 'container id'}
     * @param (array) headers         : ['header_1', 'header_2', ... 'header_n']
     * @param (array) table_details   : ['row #' : {'header text' : ['row cell 1', 'row cell 2', ... , 'row cell n']}]
     * @param (array) actions          : {'action_name': [action], ... }
     * @param (bool)  display_row_num : add column to display row number?
     */
    var TableBuilder = function(table_elements, headers, rows, actions, display_row_num, ajax_url) {
        this._ajax_url = ajax_url,

        this.headers = headers;
        this._rows = rows;

        this.num_headers = headers.length;
        this.num_rows = rows.length;

        this.num_actions = actions.length;
        this._display_row_num = display_row_num;

        this._actions = actions;

        this._container_id = table_elements.container_id;
        this._table_id = table_elements.id;
        this._table_class = table_elements.class;
    };

    //Private functions
    var extractActionNames = function(actions) {

    };

    TableBuilder.prototype = {

        constructor: TableBuilder,

        setAJAX: function(ajax_url) {
            this._ajax_url = ajax_url;
        },

        buildHeaders: function() {

            var table_headers = '<thead><tr>';

            if(this._display_row_num == true) {
                table_headers += '<th>Row #</th>';
            }

            var num_headers = this.num_headers;
            for(i = 0; i < num_headers; i++) {
                table_headers += '<th>'+this.headers[i]+'</th>';
            }

            var num_actions = this.num_actions;
            for(i = 0; i < num_actions; i++) {
                table_headers += '<th>'+this._action_names[i]+'</th>';
            }

            table_headers += '</tr></thead>';

            return table_headers;
        },

        buildRows: function() {

            var table_rows = '<tbody>';

            var table_details = this._rows;
            var headers = this.headers;
            var num_headers = this.num_headers;
            var num_rows = this.num_rows;
            var row;

            for(i = 0; i < num_rows; i++) {
                table_rows += '<tr>';

                if(this._display_row_num) {
                    table_rows += "<td>"+(i+1)+"</td>";
                }

                row = table_details[i];
                for(j = 0; j < num_headers; j++) {
                    table_rows += '<td>'+row[headers[j]]+'</td>';
                }

                var num_actions = this.num_actions;
                for(j = 0; j < num_actions; j++) {
                    table_rows += '<td>'+this._action_names[j]+'</td>';
                }

                table_rows += '</tr>';
            }

            table_rows += '</tbody>';

            return table_rows;

        },

        buildTable: function() {
            table = '<table id="'+this._table_id+'" class="'+this._table_class+'">';

            table += this.buildHeaders();
            table += this.buildRows();

            table += '</table>';

            return table;
        },

        /***
         * Attach table to containing element
         */
        attachTable: function() {
            var table = this.buildTable();

            $('#'+this._container_id).html(table);
        },

        attachAction: function(action_name, action) {

        },

        removeAction: function(action_name) {

        }
        
    };

    return TableBuilder;
})();