TableBuilderJS

A html table generator. For use by itself or with other libraries, such as [DataTables](https://datatables.net "DataTables, Table plug-in for jQuery")

Requires jQuery.


